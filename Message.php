<?php
class Message {
    /**
     * @var 短信通道的接口地址
     */
	const URL = "your post url";

    /**
     * @var 短信通道的账户名
     */
	const ACCOUNT = "your account name";

    /**
     * @var 短信通道的账户密码 
     */
    const PASSWORD = "yout account password";

    /**
     * 发送短信的函数
     *
     * @param $phonenumber 用户的手机号码
     * @param $messageContent 短信内容
     * @return 结合实际情况做结果处理
     */
    public function sendTo($phonenumber, $messageContent) {
    	return $back = $this->curlPost($phonenumber, $messageContent);
    	//不同的接口放返回的数据格式不一样，如果返回的是XML
        //可以使用simplexml_load_string($back)，将数据转化为php对象;
        //如果是json，使用json_decode($back)转化为php对象
        //然后根据通道提供方的信息规范，读取返回信息，做逻辑判断。
    }

    /**
     * 采用curl模拟post提交，发送短信
     *
     * @param $phonenumber 用户的手机号码
     * @param $messageContent 短信内容
     * @return 返回短信通方的数据,可能是json,可能是xml等等
     */
    protected function curlPost($phonenumber, $messageContent) {
    	$data = "account=" . self::ACCOUNT
    	    . "&password=" . self::PASSWORD
    	    . "&mobile=" . $phonenumber
    	    . "&content=".rawurlencode($messageContent);

    	$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, self::URL);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$return_str = curl_exec($curl);
		curl_close($curl);
		return $return_str;
    }
    
}
