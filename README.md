## Message

一个简单的关于短信发送的PHP函数类：（具体实现在Message.php文件）

### 使用：
    $message = new Message;

    $message->sendTo(phonenumber, message's content);

如：

    $message->sendTo("12345678901", "Hello!这是一个发送给您的短信Message!");

#### 注意：

    使用Message函数类之前，请确保您已经购买了短信通道，并已经知晓通道方提供给您的接口参数，请根据通道参数
    灵活配置。

    这里有一份曾经使用过的短信通道接口：

        http://www.ihuyi.com/upload/file/cu-fa-jie-kou.rar

    本函数类也是根据此接口实现的短信发送功能。


